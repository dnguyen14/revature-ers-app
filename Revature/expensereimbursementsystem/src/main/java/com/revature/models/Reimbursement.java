package com.revature.models;


import java.util.List;

public class Reimbursement {

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean  isApproved() {
        return isApproved;
    }

    public void setApproved(boolean b) {
        isApproved = b;
    }

    public List<Expense> getExpense() {
        return expense;
    }

    public void setExpense(List<Expense> expense) {
        this.expense = expense;
    }

    // TODO: Define Reimbursement Model
    private long id;
    private boolean isApproved = false;
    private List<Expense> expense;
}
