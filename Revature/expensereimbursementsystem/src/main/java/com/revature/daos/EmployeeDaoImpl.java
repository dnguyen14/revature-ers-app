package com.revature.daos;

import com.revature.models.Employee;
import com.revature.util.ConnectionUtil;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDaoImpl implements EmployeeDao {
    @Override
    public void create(Employee employee) {

        try(Connection connection = ConnectionUtil.getConnection()){
            connection.prepareStatement("insert into employee values (default,?,?,?,?,?)");
            ps.setString(1,employee.getEmployeeId());
            ps.setString(2,employee.getFirstName());
            ps.setString(3,employee.getLastName());
            ps.setString(4,employee.getEmail());
            ps.setString(5,employee.getAddress());

            //provide that SQL to JDBC to execute in our DB
            int rowsAffected = ps.executeUpdate(); // returns the number of rows affected
            if (rowsAffected==1){
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<Employee> getAllEmployeeData() {

        //create a connection to the database (using try with resources bc connection is Autocloseable)
        try (Connection connection = ConnectionUtil.getConnection();

            Statement statement = connection.createStatement();){
            ResultSet resultSet = statement.executeQuery("select * from employee");

            List<Employee> employees = new ArrayList<>();

            while(resultSet.next()){
                //get all data from the row
                int id = resultSet.getInt("employee_id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String email = resultSet.getString("email");
                String  address = resultSet.getString("address");
                Employee e = new Employee(id, firstName, lastName, email, address);

                //add employee objects to a list
                employees.add(e);
            }
            //return list
            return employees;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
