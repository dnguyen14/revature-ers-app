package com.revature.daos;

import com.revature.models.Employee;
import com.revature.util.ConnectionUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface EmployeeDao {

    @Override
    public void create(Employee employee) {
        // use employee object to create valid SQL statement
        //insert into employee values(); this statement is parameterized so we need a preparestatement


    }

    //getting employee(s) data from the database

    @Override
    public List<Employee> getAllEmployeeData() {
        return null;
    }

    /*
        required user stories
     */
    public Employee getEmployeeByUsernameAndPassword(String username, String password);

    /*
        bonus user stories
     */
    public List<Employee> getAllEmployees();
    public List<Employee> getGeneralEmployees();
    public List<Employee> getManagerEmployees();

}
