package com.revature.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil {
    public static Connection getConnection() throws SQLException {

        String connectionString = "jdbc:postgresql://traing-db.cvpidndnfzbo.us-east-1.rds.amazonaws" +
                ".com:5432/postgres";

        //we set up environment variables which we can access with our Java app
        String username = System.getenv("DB_USER");
        String password = System.getenv("DB_PASS");

        // the driver manager establishes a connection with our db credentials and db info
        return DriverManager.getConnection(connectionString, username, password);


    }
}
